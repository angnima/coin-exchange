import { useState, useEffect } from 'react'

import AccountBalance from "./components/AccountBalance/AccountBalance"
import CoinList from './components/CoinList/CoinList';
import Header from './components/Header/Header';

import styled from 'styled-components';

import axios from 'axios';

const DivApp = styled.div`
  text-align: center;
  background-color: rgb(57, 57, 87);
  color: #bbb;
`;

function App(props) {
  const [ balance, setBalance ] = useState(10000);
  const [ showBalance, setShowBalance ] = useState(true);
  const [ coinData, setCoinData ] = useState([]);
  
  const componentDidMount = async () => {
    let response = await axios.get("https://api.coinpaprika.com/v1/coins");
    let coinIds = response.data.slice(0, 10).map(coin => coin.id);

    const tickerUrl = "https://api.coinpaprika.com/v1/ticker/";
    const promises = coinIds.map(id => axios.get(tickerUrl + id))
    const coinData = await Promise.all(promises)
    const coinPriceData = coinData.map(function(response) {
      const coin = response.data
      return {
        key: coin.id,
        name: coin.name,
        ticker: coin.symbol,
        balance: 0,
        price: parseFloat(coin.price_usd).toFixed(3)
      }
    })
    
    setCoinData(coinPriceData)
  }

  useEffect(() => {
    if(coinData.length == 0) {
      componentDidMount()
    }else {
      
    }
  });

  const handleRefresh = async (v_ticker) =>  {
    const newCoinData = coinData.map(function( {key, name, ticker, price, balance} ) {
      let newPrice = price;
      // debugger;
      if(v_ticker == ticker) {
        const tickerUrl = "https://api.coinpaprika.com/v1/ticker/" + key;
        axios.get(tickerUrl)
          .then(response => {
            const coin = response.data
            newPrice = coin.price_usd
          })
      }

      return {
        key: key,
        name: name,
        ticker: ticker,
        balance: parseFloat(balance),
        price: parseFloat(newPrice).toFixed(3)
      }
    })

    setCoinData(newCoinData)
  }
  const handleBalanceState = () => {
    setShowBalance(oldValue => !oldValue);
  }
  
  return (
    <DivApp>
      
      <Header />

      <AccountBalance 
        amount={balance} 
        showBalance={showBalance} 
        handleBalance={handleBalanceState} 
      />
      
      <CoinList 
        coinData={coinData} 
        showBalance={showBalance} 
        handleRefresh={handleRefresh}
      />
    
    </DivApp>
  );
    
}

export default App;
