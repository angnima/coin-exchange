import React, { Component } from 'react'
import logo from './logo.svg';

import styled from 'styled-components';

const MyHeader = styled.header`
    background-color: #282c34;
    min-height: 20vh;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    font-size: 36px;
    color: white;
`;

const MyLogo = styled.img`
    height: 7rem;
    pointer-events: none;
`;

const Title = styled.h1`
    font-size: 2.5rem;
`;

export default class Header extends Component {
  render() {
    return (
        <MyHeader>
          <MyLogo src={logo} alt="logo" />
          <Title>
            Coin Exchange
          </Title>
        </MyHeader>
    )
  }
}
