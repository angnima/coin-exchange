import PropTypes from "prop-types"
import styled from "styled-components"

const Section = styled.section`
    font-size: 1.5rem;
    text-align: left;
    padding: 1.5rem 0 1.5rem 5rem;
`;

export default function AccountBalance(props) {
    // constructor(props) {
    //     super(props);
    //     this.handleClick = this.handleClick.bind(this);
    // }
    const handleClick = (event) => {
        event.preventDefault();
        props.handleBalance()
    }
    
    const buttonText = props.showBalance ? "Hide Balance" : "Show Balance"
    return (
        <Section>
            Balance: ${ (props.showBalance) ? props.amount : "*****" }
            <button type="button" onClick={handleClick}>{ buttonText }</button>
        </Section>
    );

}

AccountBalance.propTypes = {
    amount: PropTypes.number.isRequired
}