import PropTypes from 'prop-types'
import styled from 'styled-components'

const TD = styled.td`
border: 1px solid #ccc;
width: 20vh;
`;

export default function Coin(props) {
    // constructor(props) {
    //     super(props);
    //     this.handleClick = this.handleClick.bind(this);
    // }
    const handleClick = (event) => {
        event.preventDefault();
        props.handleRefresh(props.ticker);
    }
    
    let totalPrice = (props.price * props.balance).toFixed(2)
    if(!props.showBalance)
        totalPrice = "*****"
    return (
        <tr className="coin-row">
            <TD>{ props.name }</TD>
            <TD>{ props.ticker }</TD>
            <TD>${ props.price }</TD>
            <TD>${ totalPrice }</TD>
            <TD>
                <form action="#" method="POST">
                    <button onClick={handleClick}>
                        Refresh
                    </button>
                </form>
            </TD>
        </tr>
    )
}

Coin.propTypes = {
    name    : PropTypes.string.isRequired,
    ticker  : PropTypes.string.isRequired,
    price   : PropTypes.number.isRequired
}