import Coin from "../Coin/Coin";
import styled from 'styled-components'

const Table = styled.table`
    margin:50px auto;
    display: inline-block;
`;

export default function CoinList(props) {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         coinData: props.coinData
    //     }
    // }
    const data = props.coinData.map(({name, ticker, price, balance})=>{
        return <Coin 
                    key={ticker}
                    handleRefresh={props.handleRefresh}
                    name={ name } 
                    ticker={ticker} 
                    price={parseFloat(price)} 
                    balance={balance}
                    showBalance={props.showBalance}
                />
    })

    return (
        <Table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Ticker</th>
                <th>Price</th>
                <th>Balance</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                {data}
            </tbody>
        </Table>
    )
}

